<?php
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Spekkionu\ZendAcl\Facades\Acl;


class Register extends Eloquent implements UserInterface, RemindableInterface  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
 
 	protected $fillable = array('firstname', 'lastname', 'email','password','adress','phone','city','state','zip','country','avatar','website');
  
        public static $rules = array(
		'firstname' => 'required|min:5',
		'email' => 'required|email'
	);


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
        public function getRememberToken()
       {
           return $this->remember_token;   
       }

       public function setRememberToken($value)
       {
           $this->remember_token = $value;
       } 

       public function getRememberTokenName()
       {
           return 'remember_token';
       } 
        public function getRoleId()
    {
        return $this->role;
    }
}