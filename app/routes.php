<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

 
Route::get('home', 'HomeController@showWelcome');
Route::get('listing', 'HomeController@showListing');
 Route::get('register', function() {
 
    return View::make('register');
});
 

Route::get('classifieds', function() {
  return View::make('classifieds');
});

Route::get('deals', function() {
 
    return View::make('deals');
});

Route::get('contact', function() {
 	 
    return View::make('contact');
	
});

Route::get('admin', function() {
 	return View::make('admin.index');
	
});
Route::get('admin/ads', function() {
 	return View::make('admin.general');
	
});


Route::get('login', array('uses' => 'UserController@showLogin'));
Route::post('login', array('uses' => 'UserController@doLogin'));
//Route::post('login', array('uses' => 'UserController@createUser'));



 

