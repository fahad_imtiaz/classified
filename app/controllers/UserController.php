<?php

class UserController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function destroy() {
        Auth::logout();
        return Redirect::to('login');
    }

    public function showWelcome() {
        return View::make('hello');
    }

    public function showLogin() {
        
        return View::make('login');
    }

    public function doLogin() {
	 
		 
		$rules = array(
			'email'    => 'required|email',  
			'password' => 'required|min:3'  
		);

		 
		$validator = Validator::make(Input::all(), $rules);
 
		 
		if ($validator->fails()) {
			  
			return Redirect::to('login')
				->withErrors($validator)  
				->withInput(Input::except('password')); 
		} else {
			 
			 
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);
			 
			
	   if (Auth::attempt(Input::only('email', 'password'))) {
				  
				return Redirect::to('admin');

			} else {
				 	 	
				return Redirect::to('login');
		 
			}

		}
		
		}
		  public function createUser() {
      
	 
	 
	 	$rules = array(
			'firstname' => 'required', 
			'lastname'  => 'required', 
			'email'     => 'required|email',  
			'password'  => 'required|min:3',
			'repassword' => 'required|min:3',
			'adress'    => 'required',
			'country'   => 'required'
		);

		 $validator = Validator::make(Input::all(), $rules);
 
 
       $user = new Register;
       $user->firstname 	= Input::get('firstname');
       $user->lastname 	 = Input::get('lastname');
	   $user->email 		= Input::get('email');
	   $user->password 	 = Input::get('password');
	   $repassword	     = Input::get('repassword');
	   $user->adress 	   = Input::get('adress');
	   $user->phone 		= Input::get('phone');
	   $user->city 		 = Input::get('city');
	   $user->state 		= Input::get('state');
	   $user->zip 		  = Input::get('zip');
	   $user->country 	  = Input::get('country');
	   $user->image 	   = Input::get('image');
	   $user->website 	  = Input::get('website');
	   
	 
	   if ($validator->fails())
		{
			return Redirect::to('register')
				->withErrors($validator)  
				->withInput(Input::except('password')); 
				}
	   else if($user->password === $repassword)
	   {
		   
		  $target_path = "uploads/";
	$target_path = $target_path . basename( $_FILES['uploadedfile']['name']); 
	if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
    echo "The file ".  basename( $_FILES['uploadedfile']['name']). 
    " has been uploaded";
	} else{
    echo "There was an error uploading the file, please try again!";
} 
       




		   //$user->save();
		   }
	   else 
	   {
		   return Redirect::to('register')->with('message', 'Login Failed');
		   
		   }
			   
 	 


          
     
     
    
    }
		 

}
