@extends('layouts.classified')
@section('main')
		<div class="container">
			<div class="row">
				<div class="col-md-9">                   
					<form class="search_top_form form-inline" role="form">							
						<div class="col-md-3 col-sm-3">
							<input type="text" class="form-control" value="" placeholder="Keyword...">
						</div>
						<div class="col-md-3 col-sm-3">
							<select name="type" class="form-control">
								<option value="1">Auto</option>
								<option value="2">Beauty and Fitness</option>
								<option value="3">Real Estate</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<select name="city" class="form-control">
								<option value="1">All city...</option>
								<option value="2">Da Nang</option>
								<option value="3">Ho Chi Minh</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<button class="btn btn-warning"><span class="glyphicon glyphicon-search"></span> Search Now</button>
						</div>
					</form>
					<div class="main_content">						
						<h3 class="title"><span class="pull-left">FEATURED DEALS</span><span class="pull-right"><a href="view_all.html">View all deals</a></span></h3>						
						<div class="row">
							<div class="col-md-7">
								<div class="featured-item">
									<div class="pull-left deal-tag-info">
										<div class="deal-tag">$54</div>
										<div class="deal-discount">55% OFF</div>
									</div>
									<div class="pull-right">
										<a href="detail.html"><img src="img/products/4_photo_2916.jpg" alt="55% off - The Prettiest Beauty Parlon" title="55% off - The Prettiest Beauty Parlon" class="img deal-featured"></a>
										<p><strong><a href="detail.html" title="55% off - The Prettiest Beauty Parlon">55% off - The Prettiest Beauty Parlon</a></strong>
										<br/>by <a href="#" title="The Prettiest Beauty Parlon">The Prettiest Beauty Parlon</a></p>
									</div>
								</div>
							</div>
							<div class="col-md-5">
								<div class="other_info">
									<div class="item">
										<div class="deal-tag">$70</div>
										<strong><a href="detail.html" title="70% OFF for satellite T.V. at Soft Bed Hotel">70% OFF for satellite T.V. at Soft Bed Hotel</a></strong>
										<br/>by <a href="#" title="Soft Bed Hotel">Soft Bed Hotel</a>
									</div>
									<div class="item">
										<div class="deal-tag">$50</div>
										<strong><a href="detail.html" title="$50 - one month - Workout Gym">$50 - one month - Workout Gym</a></strong>
										<br/>by <a href="#" title="Workout">Workout</a>
									</div>
									<div class="item">
										<div class="deal-tag">$23</div>
										<strong><a href="detail.html" title="$23 For 30 Minute Massage">$23 For 30 Minute Massage</a></strong>
										<br/>by <a href="#" title="Relaxation Spa">Relaxation Spa</a>
									</div>
									<div class="item">
										<div class="deal-tag">$60</div>
										<strong><a href="detail.html" title="Weekday Promotion - 60% OFF">Weekday Promotion - 60% OFF</a></strong>
										<br/>by <a href="#" title="Golf Club Resort">Golf Club Resort</a>
									</div>
								</div>
							</div>
						</div>
						<h3 class="title"><span class="pull-left">BROWSE BY CATEGORY</span></h3>
						<div class="row listing_sub_categories">
							<div class="col-md-3"><a href="view_all.html">» Auto</a><span>(35)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Beauty and Fitness</a><span>(4419)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Entertainment</a><span>(13082)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Food and Dining</a><span>(4998)</span></div>
						</div>
						<div class="row listing_sub_categories">
							<div class="col-md-3"><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Health</a><span>(24)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Lawyer</a><span>(5)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Real Estate</a><span>(1)</span></div>
						</div>
						<div class="row listing_sub_categories">
							<div class="col-md-3"><a href="view_all.html">» Sports</a><span>(2022)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Travel</a><span>(3204)</span></div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<h3 class="title nm">BROWSE BY CATEGORY</h3>
					<div class="panel-group right-col" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<h3 class="title nm nl">Listings</h3>
								</a>
							</div>
							<div id="collapseOne" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">
										<li><a href="view_all.html">» Auto</a><span>(35)</span></li>
										<li><a href="view_all.html">» Beauty and Fitness</a><span>(4419)</span></li>
										<li><a href="view_all.html">» Entertainment</a><span>(13082)</span></li>
										<li><a href="view_all.html">» Food and Dining</a><span>(4998)</span></li>
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
										<li><a href="view_all.html">» Sports</a><span>(2022)</span></li>
										<li><a href="view_all.html">» Travel</a><span>(3204)</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									<h3 class="title nl nm">Classifieds</h3>
								</a>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">
										<li><a href="view_all.html">» Food and Dining</a><span>(4998)</span></li>
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
										<li><a href="view_all.html">» Sports</a><span>(2022)</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									<h3 class="title nl nm">Deals</h3>
								</a>
							</div>
							<div id="collapseThree" class="panel-collapse collapse in">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">																
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>			
					<h3 class="title nmb">RANDOM ITEMS</h3>
					<div id="myCarousel" class="carousel slide home">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img alt="" src="img/products/sitemgr_photo_360.jpg" />
                                <div class="carousel-caption">									
                                    <h4>Save 30%</h4>
                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                </div>
                            </div>
							<div class="item">
                                <img alt="" src="img/products/sitemgr_photo_2627.jpg" />
                                <div class="carousel-caption">
                                    <h4>Save 12%</h4>
                                    <p>Sed ut perspiciatis unde omnis iste.</p>
                                </div>
                            </div>
                        </div>
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">
						  <span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
						  <span class="glyphicon glyphicon-chevron-right"></span>
						</a>
                    </div>
					<h3 class="title nmb">RECENT REVIEWS</h3>
					<div class="reviews-item">
						<div>
							<a href="#" title="John"><img class="img" src="img/products/1_photo_1776.jpg" alt="" title=""></a>
						</div>
						<strong><a href="#">Ann Hotel</a></strong>
						<div class="rate">
							<img src="img/products/img_rateMiniStarOn.png" alt="Star On">
							<img src="img/products/img_rateMiniStarOn.png" alt="Star On">
							<img src="img/products/img_rateMiniStarOff.png" alt="Star Off">
							<img src="img/products/img_rateMiniStarOff.png" alt="Star Off">
							<img src="img/products/img_rateMiniStarOff.png" alt="Star Off">
						</div>
						<a href="#">» Read More</a>
						<p>The staff are the best! The rooms were clean! The parking was great!I had a great stay there! Can't wait to come back!!</p>
						<div class="info">
							<p>by&nbsp;<a href="#" title="John">John</a>
							<br/>Arlington, VA, 08/05/2011 - 03:37 pm</p>
						</div>
					</div>									
				</div>
			</div>			
		</div>
		
 
@if ($errors->any())
<ul>
  {{ implode('', $errors->all('
  <li class="error">:message</li>
  ')) }}
</ul>
@endif
     @stop 