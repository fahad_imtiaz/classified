<!DOCTYPE html>
<html>
	
<!-- Mirrored from www.captheme.com/bootstrap/edirectory/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Aug 2014 06:23:39 GMT -->
<head>
		<meta charset="utf-8">
		<title>Homepage | eDirectory</title>
		<meta name="description" content="">
		<meta name="author" content="cuongv">
		<meta name="robots" content="index, follow">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
				
		<!-- CSS styles -->
        {{ HTML::style('css/bootstrap.min.css') }}
        {{ HTML::style('css/main.css') }}
        {{ HTML::style('css/owl.carousel.css') }}
        {{ HTML::style('css/owl.theme.css') }}
		
		<!-- JS Libs -->
         <script src="{{ URL::asset('js/jquery.js') }}"></script>
         <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
         <script src="{{ URL::asset('js/respond.min.js') }}"></script>
         <script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
    </head>
	<body>
		<!-- Main page container -->
		<div id="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a class="brand pull-left" href="index-2.html">{{ HTML::image("img/logo.png", "") }}</a>
						<ul class="nav nav-pills pull-right">
							<li class="phone">{{ HTML::image("img/phone.png", "") }}</li>
							<li><a href="login"><i class="icon-user icon-white"></i> Sign In</a></li>
							<li><a href="register"><i class="icon-edit icon-white"></i> Create your profile</a></li>
						</ul>									
					</div>
				</div>
			</div>
		</div>		
		<div class="navbar bs-docs-nav" role="banner">
			<div class="container">
				<div class="navbar-header">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
				<ul class="nav navbar-nav">
					<li class="active"><a href="home">Home</a></li>
					<li><a href="listing">Listings</a></li>						
					<li><a href="classifieds">Classifieds</a></li>								
					<li><a href="deals">Deals</a></li>
					<li><a href="#">Articles</a></li>								
					<li><a href="#">Blog</a></li>								
					<li><a href="contact">Contact Us</a></li>
				</ul>
			</nav>
		  </div>
		</div>	
		
         @yield('main')
         
         
         
		<div id="footer">
			<div class="container">			
				<div class="row">					
					 <div class="col-md-3">   
						<h4 class="">Information</h4>
						<ul>
							<li><a href="#">About Us</a></li>
							<li><a href="#">Delivery Information</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms &amp; Conditions</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<h4>My Account</h4>
						<ul>
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order History</a></li>
							<li><a href="#">Wish List</a></li>
							<li><a href="#">Newsletter</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<h4>Connect with us</h4>
						<a href="#"> {{ HTML::image("img/social-facebook.png", "") }}</a>
						<a href="#"> {{ HTML::image("img/social-twitter.png", "") }}</a>
						<a href="#"> {{ HTML::image("img/social-rss.png", "") }}</a>
						<a href="#"> {{ HTML::image("img/social-flickr.png", "") }} </a>
					</div>
					<div class="col-md-3">
						<div class="company_info">
							<h4>eDirectory</h4>
							<p>
								74/6 Nguyen Van Thoai, Da Nang, Viet Nam<br/>
								Tel: +84 905276796
							</p>
						</div>
					</div>					
				</div>	
			</div>
		</div>
		<!-- /Main page container -->		
		<script>
			$('.carousel').carousel({
				interval: 2000
			})
			
			$("#owl-carousel").owlCarousel({
				autoPlay: 3000, //Set AutoPlay to 3 seconds
				items : 4,
				pagination : true
			});

		</script>
	</body>	

<!-- Mirrored from www.captheme.com/bootstrap/edirectory/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Aug 2014 06:24:12 GMT -->
</html>