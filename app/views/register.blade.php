@extends('layouts.classified')
@section('main')	
		<div class="container">
			<div class="row">
				<div class="col-md-9">                   
					<form class="search_top_form form-inline" role="form">							
						<div class="col-md-3 col-sm-3">
							<input type="text" class="form-control" value="" placeholder="Keyword...">
						</div>
						<div class="col-md-3 col-sm-3">
							<select name="type" class="form-control">
								<option value="1">Auto</option>
								<option value="2">Beauty and Fitness</option>
								<option value="3">Real Estate</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<select name="city" class="form-control">
								<option value="1">All city...</option>
								<option value="2">Da Nang</option>
								<option value="3">Ho Chi Minh</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<button class="btn btn-warning"><span class="glyphicon glyphicon-search"></span> Search Now</button>
						</div>
					</form>
					<div class="main_content">						
						<h3 class="title"><span class="pull-left"> REGISTER</span></h3>						
						<div class="row">
                          <h4>New Customer</h4>
								<p>By creating an account you will be able to shop faster</p> 
                                <div class="container">
                         @if ($errors->any())
                    <ul>
                        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                    </ul>
                    @endif

      							  @if(Session::has('message'))
         						   <p class="alert">{{ Session::get('message') }}</p>
        						  @endif
    							</div>
								{{ Form::open(array('action' => 'UserController@createUser')) }}
                            		<div class="col-md-6">
									<div class="form-group">
								{{ Form::label('firstname', 'First Name')}}
								{{ Form::text('firstname', null, array('class'=>'form-control', 'placeholder'=>'First Name')) }}
									</div>
									<div class="form-group">
                                    {{ Form::label('lastname', 'Last Name')}}
									{{ Form::text('lastname', null, array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
										 
									</div>
									<div class="form-group">
									 {{ Form::label('email', 'Email')}}
									{{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email')) }}
									</div>
									<div class="form-group">
									{{ Form::label('password', 'Password')}}
									{{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
									</div>
									<div class="form-group">
									 {{ Form::label('repassword', 'Re Password')}}
									{{ Form::password('repassword', array('class'=>'form-control', 'placeholder'=>'Re Password')) }}
									</div>									
								 <div class="form-group">
                                    {{ Form::label('adress', 'Adress')}}
									{{ Form::text('adress', null, array('class'=>'form-control', 'placeholder'=>'Adress')) }}
									</div>
									<div class="form-group">
									{{ Form::label('phone', 'Phone')}}
									{{ Form::text('phone', null, array('class'=>'form-control', 'placeholder'=>'Phone')) }}
									</div>
								 	</div>
                            <div class="col-md-6">
								 	<div class="form-group">
									{{ Form::label('city', 'City')}}
									{{ Form::text('city', null, array('class'=>'form-control', 'placeholder'=>'City')) }}
									</div>
									<div class="form-group">
									{{ Form::label('state', 'State')}}
									{{ Form::text('state', null, array('class'=>'form-control', 'placeholder'=>'State')) }}
									</div>	
                                    <div class="form-group">
									{{ Form::label('zip', 'Zip')}}
									{{ Form::text('zip', null, array('class'=>'form-control', 'placeholder'=>'Zip')) }}
									</div>
                                    <div class="form-group">
									{{ Form::label('country', 'Country')}}
									{{ Form::text('country', null, array('class'=>'form-control', 'placeholder'=>'Country')) }}
									</div>
                                    <div class="form-group">
									 {{ Form::label('image', 'Profile Pic')}}
                                     <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                                     <input name="uploadedfile" type="file" />
									</div>
                                    <div class="form-group">
									{{ Form::label('website', 'Website')}}
									{{ Form::text('website', null, array('class'=>'form-control', 'placeholder'=>'Website')) }}
									</div>
                                     <div class="form-group">
                                    {{ Form::checkbox('name', 'value', true);}}
                                     I Accept the Terms and Condition
                                    </div>								
									<div class="form-group">										
									 {{ Form::submit('Register', array('class'=>'btn btn-primary'))}}									
									</div>
                             </div>
                            {{ Form::close() }}
						</div>						
					</div>
				</div>
				<div class="col-md-3">
					<h3 class="title nm">BROWSE BY CATEGORY</h3>
					<div class="panel-group right-col" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<h3 class="title nm nl">Listings</h3>
								</a>
							</div>
							<div id="collapseOne" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">
										<li><a href="view_all.html">» Auto</a><span>(35)</span></li>
										<li><a href="view_all.html">» Beauty and Fitness</a><span>(4419)</span></li>
										<li><a href="view_all.html">» Entertainment</a><span>(13082)</span></li>
										<li><a href="view_all.html">» Food and Dining</a><span>(4998)</span></li>
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
										<li><a href="view_all.html">» Sports</a><span>(2022)</span></li>
										<li><a href="view_all.html">» Travel</a><span>(3204)</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									<h3 class="title nl nm">Classifieds</h3>
								</a>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">
										<li><a href="view_all.html">» Food and Dining</a><span>(4998)</span></li>
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
										<li><a href="view_all.html">» Sports</a><span>(2022)</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									<h3 class="title nl nm">Deals</h3>
								</a>
							</div>
							<div id="collapseThree" class="panel-collapse collapse in">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">																
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>			
					<h3 class="title nmb">RANDOM ITEMS</h3>
					<div id="myCarousel" class="carousel slide home">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img alt="" src="img/products/sitemgr_photo_360.jpg" />
                                <div class="carousel-caption">									
                                    <h4>Save 30%</h4>
                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                </div>
                            </div>
							<div class="item">
                                <img alt="" src="img/products/sitemgr_photo_2627.jpg" />
                                <div class="carousel-caption">
                                    <h4>Save 12%</h4>
                                    <p>Sed ut perspiciatis unde omnis iste.</p>
                                </div>
                            </div>
                        </div>
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">
						  <span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
						  <span class="glyphicon glyphicon-chevron-right"></span>
						</a>
                    </div>
					<h3 class="title nmb">RECENT REVIEWS</h3>
					<div class="reviews-item">
						<div>
							<a href="#" title="John"><img class="img" src="img/products/1_photo_1776.jpg" alt="" title=""></a>
						</div>
						<strong><a href="#">Ann Hotel</a></strong>
						<div class="rate">
							<img src="img/products/img_rateMiniStarOn.png" alt="Star On">
							<img src="img/products/img_rateMiniStarOn.png" alt="Star On">
							<img src="img/products/img_rateMiniStarOff.png" alt="Star Off">
							<img src="img/products/img_rateMiniStarOff.png" alt="Star Off">
							<img src="img/products/img_rateMiniStarOff.png" alt="Star Off">
						</div>
						<a href="#">» Read More</a>
						<p>The staff are the best! The rooms were clean! The parking was great!I had a great stay there! Can't wait to come back!!</p>
						<div class="info">
							<p>by&nbsp;<a href="#" title="John">John</a>
							<br/>Arlington, VA, 08/05/2011 - 03:37 pm</p>
						</div>
					</div>									
				</div>
			</div>			
		</div>

@stop

