@extends('layouts.classified')
@section('main')
		<div class="container">
			<div class="row">
				<div class="col-md-9">                   
					<form class="search_top_form form-inline" role="form">							
						<div class="col-md-3 col-sm-3">
							<input type="text" class="form-control" value="" placeholder="Keyword...">
						</div>
						<div class="col-md-3 col-sm-3">
							<select name="type" class="form-control">
								<option value="1">Auto</option>
								<option value="2">Beauty and Fitness</option>
								<option value="3">Real Estate</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<select name="city" class="form-control">
								<option value="1">All city...</option>
								<option value="2">Da Nang</option>
								<option value="3">Ho Chi Minh</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<button class="btn btn-warning"><span class="glyphicon glyphicon-search"></span> Search Now</button>
						</div>
					</form>
					<div class="main_content">						
						<h3 class="title"><span class="pull-left">CONTACT US</span></h3>
						<div class="row">
							<div class="col-md-12">
								<iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=74%2F6+Nguy%E1%BB%85n+V%C4%83n+Tho%E1%BA%A1i,+S%C6%A1n+Tr%C3%A0,+%C4%90%C3%A0+N%E1%BA%B5ng,+Vi%E1%BB%87t+Nam&amp;aq=0&amp;oq=74%2F6+Nguyen+Van+Thoai+Da+Nang,+Viet+Nam&amp;sll=37.0625,-95.677068&amp;sspn=41.546728,79.013672&amp;ie=UTF8&amp;hq=&amp;hnear=74+Nguy%E1%BB%85n+V%C4%83n+Tho%E1%BA%A1i,+Ng%C5%A9+H%C3%A0nh+S%C6%A1n,+Da+Nang,+Vietnam&amp;t=m&amp;ll=16.064537,108.24151&amp;spn=0.032992,0.039396&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-md-5">
								<h4>ADDITIONAL INFORMATION</h4>
								<p><strong>Phone:</strong>&nbsp;(123) 456-7890<br>
								<strong>Fax:</strong>&nbsp;+04 (123) 456-7890<br>
								<strong>Email:</strong>&nbsp;<a href="#">vietcuong_it@yahoo.com</a>								
								</p>
								<br/>
								<h5>SECONDARY OFFICE IN VIETNAM</h5>
								<p><strong>Phone:</strong>&nbsp;(113) 023-1125<br>
								<strong>Fax:</strong>&nbsp;+04 (113) 023-1145<br>
								<strong>Email:</strong>&nbsp;<a href="#">vietcuong_it@yahoo.com</a>					
								</p>
							</div>
							<div class="col-md-7">
								<h4>CONTACT FORM</h4>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
								<form role="form">
									<div class="form-group">
										<label for="inputName">Name</label>
										<input type="email" class="form-control" id="inputName" placeholder="Name">
									</div>
									<div class="form-group">
										<label for="inputEmail">Email</label>
										<input type="email" class="form-control" id="inputEmail" placeholder="Email">
									</div>
									<div class="form-group">										
										<textarea class="form-control" rows="5"></textarea>					
									</div>
									<div class="form-group">										
										<button type="submit" class="btn btn-primary">Send message</button>
									</div>
								</form>
							</div>
						</div>						
					</div>
				</div>
				<div class="col-md-3">
					<h3 class="title nm">BROWSE BY CATEGORY</h3>
					<div class="panel-group right-col" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<h3 class="title nm nl">Listings</h3>
								</a>
							</div>
							<div id="collapseOne" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">
										<li><a href="view_all.html">» Auto</a><span>(35)</span></li>
										<li><a href="view_all.html">» Beauty and Fitness</a><span>(4419)</span></li>
										<li><a href="view_all.html">» Entertainment</a><span>(13082)</span></li>
										<li><a href="view_all.html">» Food and Dining</a><span>(4998)</span></li>
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
										<li><a href="view_all.html">» Sports</a><span>(2022)</span></li>
										<li><a href="view_all.html">» Travel</a><span>(3204)</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									<h3 class="title nl nm">Classifieds</h3>
								</a>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">
										<li><a href="view_all.html">» Food and Dining</a><span>(4998)</span></li>
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
										<li><a href="view_all.html">» Sports</a><span>(2022)</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									<h3 class="title nl nm">Deals</h3>
								</a>
							</div>
							<div id="collapseThree" class="panel-collapse collapse in">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">																
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>			
					<h3 class="title nmb">RANDOM ITEMS</h3>
					<div id="myCarousel" class="carousel slide home">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img alt="" src="img/products/sitemgr_photo_360.jpg" />
                                <div class="carousel-caption">									
                                    <h4>Save 30%</h4>
                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                </div>
                            </div>
							<div class="item">
                                <img alt="" src="img/products/sitemgr_photo_2627.jpg" />
                                <div class="carousel-caption">
                                    <h4>Save 12%</h4>
                                    <p>Sed ut perspiciatis unde omnis iste.</p>
                                </div>
                            </div>
                        </div>
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">
						  <span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
						  <span class="glyphicon glyphicon-chevron-right"></span>
						</a>
                    </div>
					<h3 class="title nmb">RECENT REVIEWS</h3>
					<div class="reviews-item">
						<div>
							<a href="#" title="John"><img class="img" src="img/products/1_photo_1776.jpg" alt="" title=""></a>
						</div>
						<strong><a href="#">Ann Hotel</a></strong>
						<div class="rate">
							<img src="img/products/img_rateMiniStarOn.png" alt="Star On">
							<img src="img/products/img_rateMiniStarOn.png" alt="Star On">
							<img src="img/products/img_rateMiniStarOff.png" alt="Star Off">
							<img src="img/products/img_rateMiniStarOff.png" alt="Star Off">
							<img src="img/products/img_rateMiniStarOff.png" alt="Star Off">
						</div>
						<a href="#">» Read More</a>
						<p>The staff are the best! The rooms were clean! The parking was great!I had a great stay there! Can't wait to come back!!</p>
						<div class="info">
							<p>by&nbsp;<a href="#" title="John">John</a>
							<br/>Arlington, VA, 08/05/2011 - 03:37 pm</p>
						</div>
					</div>									
				</div>
			</div>			
		</div>
	@if ($errors->any())
<ul>
  {{ implode('', $errors->all('
  <li class="error">:message</li>
  ')) }}
</ul>
@endif
     @stop 