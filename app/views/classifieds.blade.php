@extends('layouts.classified')
@section('main')	
		<div class="container">
			<div class="row">
				<div class="col-md-9">                   
					<form class="search_top_form form-inline" role="form">							
						<div class="col-md-3 col-sm-3">
							<input type="text" class="form-control" value="" placeholder="Keyword...">
						</div>
						<div class="col-md-3 col-sm-3">
							<select name="type" class="form-control">
								<option value="1">Auto</option>
								<option value="2">Beauty and Fitness</option>
								<option value="3">Real Estate</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<select name="city" class="form-control">
								<option value="1">All city...</option>
								<option value="2">Da Nang</option>
								<option value="3">Ho Chi Minh</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<button class="btn btn-warning"><span class="glyphicon glyphicon-search"></span> Search Now</button>
						</div>
					</form>
					<div class="main_content">						
						<h3 class="title"><span class="pull-left">FEATURED CLASSIFIEDS</span><span class="pull-right"><a href="view_all.html">View all classifieds</a></span></h3>
						<div class="row">
							<div class="col-md-7">
								<div class="featured-item">
									<a href="detail.html"><img src="img/products/19_photo_2985.jpg" alt="Sell Harley-Davidson" title="Sell Harley-Davidson" class="img pull-left"></a>
									<strong><a href="detail.html">Sell Harley-Davidson</a> <span class="label label-danger">$312</span></strong>
									<p>Published: 05/12/2010 by <a href="#" target="_blank"> Olivia Matthew</a> in <a href="#">Finance</a></p>
									<p>This is example text for the overview section. This is my boss, Jonathan Hart, a self-made millionaire, he's quite a guy</p>
								</div>
								<div class="featured-item">
									<a href="detail.html"><img src="img/products/1_photo_2941.jpg" alt="Setup your Table for Special Events" title="Setup your Table for Special Events" class="img pull-left"></a>
									<strong><a href="detail.html">Setup your Table for Special Events</a></strong>
									<p>Published: 01/24/2010 by <a href="#" target="_blank"> Anthony Mark</a> in <a href="#">Finance</a></p>
									<p>This is example text for the overview section. One for all and all for one, Muskehounds are always ready.</p>
								</div>
								<div class="featured-item">
									<a href="detail.html"><img src="img/products/218_photo_3111.jpg" alt="Sell Harley-Davidson" title="Sell Harley-Davidson" class="img pull-left"></a>
									<strong><a href="detail.html">Golf Club Resort</a> <span class="label label-danger">$312</span></strong>
									<p>Published: 05/12/2010 by <a href="#" target="_blank"> Olivia Matthew</a> in <a href="#">Finance</a></p>
									<p>This is example text for the overview section. This is my boss, Jonathan Hart, a self-made millionaire, he's quite a guy</p>
								</div>
							</div>
							<div class="col-md-5">
								<div class="other_info">
									<p>
										<strong><a href="detail.html">Rent a commercial room</a> <span class="label label-success">$250</span></strong>
										<br/>in <a href="#">Life Style</a> - 01/01/2010					
									</p>							
									<p>
										<strong><a href="detail.html">Starting a Business</a> <span class="label label-danger">$457</span></strong>						
										<br/>in <a href="#">Business</a> - 12/26/2009					
									</p>						
									<p>
										<strong><a href="detail.html">Health for kids</a></strong>							
										<br/>in <a href="#">Fitness</a> - 12/21/2009					
									</p>						
									<p>
										<strong><a href="detail.html">The future of recycling</a></strong>
										<br/>in <a href="#">Volunteer</a> - 07/04/2009
									</p>
									<p>
										<strong><a href="detail.html">Health for kids</a></strong>							
										<br/>in <a href="#">Fitness</a> - 12/21/2009					
									</p>						
									<p>
										<strong><a href="detail.html">The future of recycling</a></strong>
										<br/>in <a href="#">Volunteer</a> - 07/04/2009
									</p>
								</div>
							</div>
						</div>
						<h3 class="title"><span class="pull-left">BROWSE BY CATEGORY</span></h3>
						<div class="row listing_sub_categories">
							<div class="col-md-3"><a href="view_all.html">» Auto</a><span>(35)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Beauty and Fitness</a><span>(4419)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Entertainment</a><span>(13082)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Food and Dining</a><span>(4998)</span></div>
						</div>
						<div class="row listing_sub_categories">
							<div class="col-md-3"><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Health</a><span>(24)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Lawyer</a><span>(5)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Real Estate</a><span>(1)</span></div>
						</div>
						<div class="row listing_sub_categories">
							<div class="col-md-3"><a href="view_all.html">» Sports</a><span>(2022)</span></div>
							<div class="col-md-3"><a href="view_all.html">» Travel</a><span>(3204)</span></div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<h3 class="title nm">BROWSE BY CATEGORY</h3>
					<div class="panel-group right-col" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<h3 class="title nm nl">Listings</h3>
								</a>
							</div>
							<div id="collapseOne" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">
										<li><a href="view_all.html">» Auto</a><span>(35)</span></li>
										<li><a href="view_all.html">» Beauty and Fitness</a><span>(4419)</span></li>
										<li><a href="view_all.html">» Entertainment</a><span>(13082)</span></li>
										<li><a href="view_all.html">» Food and Dining</a><span>(4998)</span></li>
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
										<li><a href="view_all.html">» Sports</a><span>(2022)</span></li>
										<li><a href="view_all.html">» Travel</a><span>(3204)</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									<h3 class="title nl nm">Classifieds</h3>
								</a>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse in">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">
										<li><a href="view_all.html">» Food and Dining</a><span>(4998)</span></li>
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
										<li><a href="view_all.html">» Sports</a><span>(2022)</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									<h3 class="title nl nm">Deals</h3>
								</a>
							</div>
							<div id="collapseThree" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">																
										<li><a href="view_all.html">» Gifts and Flowers</a><span>(8106)</span></li>
										<li><a href="view_all.html">» Health</a><span>(24)</span></li>
										<li><a href="view_all.html">» Lawyer</a><span>(5)</span></li>
										<li><a href="view_all.html">» Real Estate</a><span>(1)</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>				
					<h3 class="title nmb">RANDOM ITEMS</h3>
					<div id="myCarousel" class="carousel slide home">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img alt="" src="img/products/sitemgr_photo_360.jpg" />
                                <div class="carousel-caption">									
                                    <h4>Save 30%</h4>
                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                </div>
                            </div>
							<div class="item">
                                <img alt="" src="img/products/sitemgr_photo_2627.jpg" />
                                <div class="carousel-caption">
                                    <h4>Save 12%</h4>
                                    <p>Sed ut perspiciatis unde omnis iste.</p>
                                </div>
                            </div>
                        </div>
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">
						  <span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
						  <span class="glyphicon glyphicon-chevron-right"></span>
						</a>
                    </div>	
					<br/>
				</div>
			</div>			
		</div>
		@if ($errors->any())
<ul>
  {{ implode('', $errors->all('
  <li class="error">:message</li>
  ')) }}
</ul>
@endif
     @stop 