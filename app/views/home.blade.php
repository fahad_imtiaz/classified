 @extends('layouts.classified')
@section('main')	
 
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<form class="search_top_form form-inline" role="form">							
						<div class="col-md-3 col-sm-3">
							<input type="text" class="form-control" value="" placeholder="Keyword...">
						</div>
						<div class="col-md-3 col-sm-3">
							<select name="type" class="form-control">
								<option value="1">Auto</option>
								<option value="2">Beauty and Fitness</option>
								<option value="3">Real Estate</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<select name="city" class="form-control">
								<option value="1">All city...</option>
								<option value="2">Da Nang</option>
								<option value="3">Ho Chi Minh</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<button class="btn btn-warning"><span class="glyphicon glyphicon-search"></span> Search Now</button>
						</div>
					</form>
					<div class="main_content">
						<h3 class="title"><span class="pull-left">FEATURED LISTINGS</span><span class="pull-right"><a href="view_all.html">View all listings</a></span></h3>
						<div class="row">
							<div class="span12 product_listing">
								<div id="owl-carousel" class="owl-carousel">
									<div class="col-md-12">
										<div class="item">
											<a href="detail.html">{{ HTML::image("img/products/m2.png", "Products") }}</a>
											<a href="detail.html" class="title">Academy Theatre</a> <span class="label label-success">new</span><br/>in <a href="view_all.html" class="subinfo">Entertainment</a> by <a href="#" class="subinfo">Amanda</a>
											<p>This is example text for the overview section. This is my boss, Jonathan...</p>
										</div>
									</div>
									<div class="col-md-12">
										<div class="item">
											<a href="detail.html">{{ HTML::image("img/products/m3.png", "Jewelery") }}</a>
											<br/><a href="detail.html" class="title">Special Jewelry</a><br/>in <a href="view_all.html" class="subinfo">Real Estate</a> by <a href="#" class="subinfo">Anthony Bill</a>
											<p>This is example text for the overview section. There's a voice that keep...</p>
										</div>
									</div>
									<div class="col-md-12">
										<div class="item">
											<a href="detail.html">{{ HTML::image("img/products/m6.png", "Hotel") }}</a>
											<a href="detail.html" class="title">Soft Bed Hotel</a><br/>in <a href="view_all.html" class="subinfo">Travel</a> by <a href="#" class="subinfo">Julia</a>
											<p>This is example text for the overview section. Just the good ol' boys, n...</p>
										</div>
									</div>
									<div class="col-md-12">
										<div class="item">
											<a href="detail.html">{{ HTML::image("img/products/m1.png", "Fly Airline") }}</a>
											<a href="detail.html" class="title">Fly Airlines</a><br/>in <a href="view_all.html" class="subinfo">Sports</a> by <a href="#" class="subinfo">Carl Gilmore</a>
											<p>This is example text for the overview section. One for all and all for o...</p>
										</div>
									</div>
									<div class="col-md-12">
										<div class="item">
											<a href="detail.html">{{ HTML::image("img/products/m3.png", "Jewelery") }}</a>
											<br/><a href="detail.html" class="title">Special Jewelry</a><br/>in <a href="view_all.html" class="subinfo">Real Estate</a> by <a href="#" class="subinfo">Anthony Bill</a>
											<p>This is example text for the overview section. There's a voice that keep...</p>
										</div>
									</div>
									<div class="col-md-12">
										<div class="item">
											<a href="detail.html">{{ HTML::image("img/products/m2.png", "Academy Theatre") }}</a>
											<a href="detail.html" class="title">Academy Theatre</a> <span class="label label-success">new</span><br/>in <a href="view_all.html" class="subinfo">Entertainment</a> by <a href="#" class="subinfo">Amanda</a>
											<p>This is example text for the overview section. This is my boss, Jonathan...</p>
										</div>
									</div>								
									<div class="col-md-12">
										<div class="item">
											<a href="detail.html"><img class="img" src="img/products/m1.png" alt="Fly Airlines" title="Fly Airlines"></a>
											<a href="detail.html" class="title">Fly Airlines</a><br/>in <a href="view_all.html" class="subinfo">Sports</a> by <a href="#" class="subinfo">Carl Gilmore</a>
											<p>This is example text for the overview section. One for all and all for o...</p>
										</div>
									</div>
									<div class="col-md-12">
										<div class="item">
											<a href="detail.html">{{ HTML::image("img/products/m6.png", "Soft Hotel") }}</a>
											<a href="detail.html" class="title">Soft Bed Hotel</a><br/>in <a href="view_all.html" class="subinfo">Travel</a> by <a href="#" class="subinfo">Julia</a>
											<p>This is example text for the overview section. Just the good ol' boys, n...</p>
										</div>
									</div>									
								</div>								
							</div>
						</div>						
						<h3 class="title"><span class="pull-left">FEATURED DEALS</span><span class="pull-right"><a href="view_all.html">View all deals</a></span></h3>						
						<div class="row">
							<div class="col-md-7 col-sm-7">
								<div class="featured-item">
									<div class="pull-left deal-tag-info">
										<div class="deal-tag">$54</div>
										<div class="deal-discount">55% OFF</div>
									</div>
									<div class="pull-right">
										<a href="detail.html"> <img src="img/products/4_photo_2916.jpg" alt="55% off - The Prettiest Beauty Parlon" title="55% off - The Prettiest Beauty Parlon" class="img deal-featured"></a>
										<p><strong><a href="detail.html" title="55% off - The Prettiest Beauty Parlon">55% off - The Prettiest Beauty Parlon</a></strong>
										<br/>by <a href="#" title="The Prettiest Beauty Parlon">The Prettiest Beauty Parlon</a></p>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-sm-5">
								<div class="other_info">
									<div class="item">
										<div class="deal-tag">$70</div>
										<strong><a href="detail.html" title="70% OFF for satellite T.V. at Soft Bed Hotel">70% OFF for satellite T.V. at Soft Bed Hotel</a></strong>
										<br/>by <a href="#" title="Soft Bed Hotel">Soft Bed Hotel</a>
									</div>
									<div class="item">
										<div class="deal-tag">$50</div>
										<strong><a href="detail.html" title="$50 - one month - Workout Gym">$50 - one month - Workout Gym</a></strong>
										<br/>by <a href="#" title="Workout">Workout</a>
									</div>
									<div class="item">
										<div class="deal-tag">$23</div>
										<strong><a href="detail.html" title="$23 For 30 Minute Massage">$23 For 30 Minute Massage</a></strong>
										<br/>by <a href="#" title="Relaxation Spa">Relaxation Spa</a>
									</div>
									<div class="item">
										<div class="deal-tag">$60</div>
										<strong><a href="detail.html" title="Weekday Promotion - 60% OFF">Weekday Promotion - 60% OFF</a></strong>
										<br/>by <a href="#" title="Golf Club Resort">Golf Club Resort</a>
									</div>
								</div>
							</div>
						</div>
						<h3 class="title"><span class="pull-left">FEATURED CLASSIFIEDS</span><span class="pull-right"><a href="view_all.html">View all classifieds</a></span></h3>
						<div class="row">
							<div class="product_listing">
								<div class="col-md-3 col-sm-6">
									<div class="item">
										<a href="detail.html">{{ HTML::image("img/products/m4.png", "Soft Hotel") }}</a>
										<a href="detail.html" class="title">Sell Harley-Davidson</a> <span class="label">$200</span><br/>in <a href="view_all.html" class="subinfo">Entertainment</a> by <a href="#" class="subinfo">Amanda</a>										
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="item">
										<a href="detail.html">{{ HTML::image("img/products/m7.png", "Winter Make -up") }}</a>
										<a href="detail.html" class="title">Winter Make-up</a> <span class="label">$370</span><br/>in <a href="view_all.html" class="subinfo">Health</a> by <a href="#" class="subinfo">Julia</a>										
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="item">
										<a href="detail.html">{{ HTML::image("img/products/m8.png", "") }}</a>
										<a href="detail.html" class="title">Office Furniture</a> <span class="label">$120</span><br/>in <a href="view_all.html" class="subinfo">Leisure</a> by <a href="#" class="subinfo">Jane</a>										
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="item">
										<a href="detail.html">{{ HTML::image("img/products/m2.png", "") }}</a>
										<a href="detail.html" class="title">BMW Deluxe</a> <span class="label label-danger">ask</span><br/>in <a href="view_all.html" class="subinfo">Food</a> by <a href="#" class="subinfo">Mark</a>										
									</div>
								</div>
							</div>
						</div>
						<h3 class="title"><span class="pull-left">RECENT ARTICLES</span><span class="pull-right"><a href="view_all.html">View all articles</a></span></h3>
						<div class="row">
							<div class="col-md-7">
								<div class="featured-item">
									<a href="#"><img src="img/products/8_photo_2993.jpg" alt="Investing in Stocks" title="Investing in Stocks" class="img pull-left"></a>
									<strong><a href="#">Investing in Stocks</a></strong>
									<p>Published: 05/12/2010 by <a href="#" target="_blank"> Olivia Matthew</a> in <a href="#">Finance</a></p>
									<p>This is example text for the overview section. This is my boss, Jonathan Hart, a self-made millionaire, he's quite a guy</p>
								</div>
								<div class="featured-item">
									<a href="#"><img src="img/products/7_photo_3011.jpg" alt="Retirement Plans" title="Retirement Plans" class="img pull-left"></a>
									<strong><a href="#">Retirement Plans</a></strong>
									<p>Published: 01/24/2010 by <a href="#" target="_blank"> Anthony Mark</a> in <a href="#">Finance</a></p>
									<p>This is example text for the overview section. One for all and all for one, Muskehounds are always ready.</p>
								</div>
							</div>
							<div class="col-md-5">
								<div class="other_info">
									<p>
										<strong><a href="#">Personal Growth	Personal Growth</a></strong>
										<br/>in <a href="#">Life Style</a> - 01/01/2010					
									</p>
							
									<p>
										<strong><a href="#">Starting a Business</a></strong>						
										<br/>in <a href="#">Business</a> - 12/26/2009					
									</p>						
									<p>
										<strong><a href="#">Health for kids</a></strong>							
										<br/>in <a href="#">Fitness</a> - 12/21/2009					
									</p>						
									<p>
										<strong><a href="#">The future of recycling</a></strong>
										<br/>in <a href="#">Volunteer</a> - 07/04/2009
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<h3 class="title nm">BROWSE BY CATEGORY</h3>
					<div class="panel-group right-col" id="accordion">
                    @foreach ($results as $result)
						<div class="panel panel-default">
                          
							<div class="panel-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<h3 class="title nm nl">{{$result->category_name}}</h3>
								</a>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in">
								<div class="panel-body">
									<ul class="nav nav-pills nav-stacked">
                                     @foreach ($listing as $listings)
                                     @if ($listings->parent_id === $result->category_id)
                                   <li>»{{$listings->category_name}} </li>
                                   @endif
                                 @endforeach
                                    </ul>
								</div>
							</div>
						</div>
						  @endforeach
					</div>			
					<h3 class="title nmb">RANDOM ITEMS</h3>
					<div id="myCarousel" class="carousel slide home">
                        <div class="carousel-inner">
                            <div class="item active">
                            {{ HTML::image("img/products/sitemgr_photo_360.jpg", "") }}
                               
                                <div class="carousel-caption">									
                                    <h4>Save 30%</h4>
                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                </div>
                            </div>
							<div class="item">
                             {{ HTML::image("img/products/sitemgr_photo_2627.jpg", "") }}
                                 
                                <div class="carousel-caption">
                                    <h4>Save 12%</h4>
                                    <p>Sed ut perspiciatis unde omnis iste.</p>
                                </div>
                            </div>
                        </div>
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">
						  <span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
						  <span class="glyphicon glyphicon-chevron-right"></span>
						</a>
                    </div>
					<h3 class="title nmb">RECENT REVIEWS</h3>
					<div class="reviews-item">
						<div>
							<a href="#" title="John"> {{ HTML::image("img/products/1_photo_1776.jpg", "") }}</a>
						</div>
						<strong><a href="#">Ann Hotel</a></strong>
						<div class="rate">
                        {{ HTML::image("img/products/img_rateMiniStarOn.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOn.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOff.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOff.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOff.png", "") }}
						 </div>
						<a href="#">» Read More</a>
						<p>The staff are the best! The rooms were clean! The parking was great!I had a great stay there! Can't wait to come back!!</p>
						<div class="info">
							<p>by&nbsp;<a href="#" title="John">John</a>
							<br/>Arlington, VA, 08/05/2011 - 03:37 pm</p>
						</div>
					</div>
					<div class="reviews-item">
						<div>
							<a href="#" title="Mark">   {{ HTML::image("img/products/218_photo_1772.jpg", "") }} </a>
						</div>
						<strong><a href="#">Mattew Law Firm</a></strong>
						<div class="rate">
							 {{ HTML::image("img/products/img_rateMiniStarOn.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOn.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOff.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOff.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOff.png", "") }}
						</div>
						<a href="#">» Read More</a>
						<p>They were responsive, attentive and friendly while helping us.</p>
						<div class="info">
							<p>by&nbsp;<a href="#" title="Mark">Mark</a>
							<br/>Tulsa, Oklahoma, 06/16/2010 - 06:26 pm</p>
						</div>
					</div>
					<div class="reviews-item">
						<div class="item">
							<a href="#" title="Julia"><img class="img" src="img/products/9_photo_1778.jpg" alt="" title=""></a>
						</div>
						<strong><a href="#">Tayu</a></strong>
						<div class="rate">
							 {{ HTML::image("img/products/img_rateMiniStarOn.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOn.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOff.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOff.png", "") }}
                        {{ HTML::image("img/products/img_rateMiniStarOff.png", "") }}
						</div>
						<a href="#">» Read More</a>
						<p>The best part of this restaurant was definitely the decor and the server. We had the agedashi tofu and it was THE BES...</p>
						<div class="info">
							<p>by&nbsp;<a href="#" title="Julia">Julia</a>
							<br/>Norwich, New York, 06/16/2010 - 06:19 pm</p>
						</div>
					</div>
				</div>
			</div>			
		</div>
		<!-- /Main page container -->		
		<script>
			$('.carousel').carousel({
				interval: 2000
			})
			
			$("#owl-carousel").owlCarousel({
				autoPlay: 3000, //Set AutoPlay to 3 seconds
				items : 4,
				pagination : true
			});
		</script>
	</body>	
<!-- Mirrored from www.captheme.com/bootstrap/edirectory/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Aug 2014 06:24:12 GMT -->
</html>
@if ($errors->any())
<ul>
  {{ implode('', $errors->all('
  <li class="error">:message</li>
  ')) }}
</ul>
@endif
     @stop 