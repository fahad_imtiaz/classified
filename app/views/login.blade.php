@extends('layouts.classified')
@section('main')

 
<br>
	<div class="container">
			<div class="row">
{{ Form::open(array('url'=>'login', 'style'=>'width:400px;')) }}
 
<h2 class="form-signin-heading">Admin Panel</h2>

{{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
<br />
{{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
<br />
{{ Form::submit('Login', array('class'=>'btn btn-large btn-primary btn-block'))}}
{{ Form::close() }}

 
</div></div>
<br />
<br />
@if ($errors->any())
<ul>
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
</ul>
@endif

@stop